# Sujet participant

## Prérequis

Pour excécuter Mindustry, vous aurez besion de [Java 17](https://www.oracle.com/java/technologies/downloads/#java17) d'intallé au préalable.

## Lancement de Mindustry

Exécutez dans un terminal le fichier [Mindustry.jar](https://github.com/Anuken/Mindustry/releases/download/v144/Mindustry.jar) grâce à la commande
```bash
java -jar Mindustry.jar
``` 

### ATTENTION WHITELIST

la première fois que vous lancez le client `Mindustry.jar`, regardez le terminal qui a lancé Mindustry et chercher la ligne où est marqué “Player UUID: XXX” et envoyer cette UUID dans votre channel discord d’équipe

## Ajout des mods et de la carte

Copiez les dossiez `mods` et `maps` dans le dossier de fichier locaux du jeu.
Pour le trouver, depuis Mindustry, allez dans `Settings -> Game Data -> Open Data Folder`.
Collez ces dossier dans le dossier `Mindustry`.
Une fois les `mods` et `maps` copiés, relancez le client `Mindustry.jar` pour qu'ils soient effectif dans le jeu.

## Connection de l'IA
Vous pouvez trouver tout les fichiers requis pour commencer votre ia dans le dossier `ia`, dans un premier temps installer les dépendances avec `pip -r ia/requirements.txt` puis compléter le fichier `ia/template.py` pour coder votre ia.

Une fois le client Mindustry lancé, excéutéz votre IA pour vous connecter au client.
Vous devriez voir les messages suivant :
```txt
Connecting to Mindustry client on port 2023...
Connection established
```

## Serveur local
Pour créer un server local, Exécutez dans un terminal le fichier [server-release.jar](https://github.com/Anuken/Mindustry/releases/download/v144/server-release.jar) grâce à la commande
```bash
java -jar server-release.jar
``` 
Pour ajouter les mods et la carte, arrétez le server (`exit`) puis collez les dossiers de mods et de cartes dans le dossier `config` qui à été crée.
Vous pouvez ensuite relancer le serveur.

Une fois le serveur lancé, faites la commande suivante pour host une partie (vous pouvez changer `pvp` par `sandbox` si besoin):
```bash
host Codinsa_2023 pvp
```

### Avec docker

Vous pouvez aussi utiliser l'image docker toute prête pour le serveur :

```shell
docker run --rm --pull always -p 6567:6567/tcp -p 6567:6567/udp -it filature.duia.eu:5000/codinsa/finale2023-server
```

Avec cette commande, votre serveur serveur sera toujours à jour avec la dernière version publiée :).
