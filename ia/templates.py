###########################################################################
##### ------------------------ CODE TEMPLATE ------------------------ #####
###########################################################################

from mindustry import *

class AI(Mindustry):

    def setup(self):
        pass # TODO

    def run(self):
        self.build(Blocks.mechanical_drill, 44, 11)
        self.build_line(Blocks.titanium_conveyor, 46, 12, 47, 12)
        self.sleep_until_built()
        while True:
            pass # TODO
            self.sleep_until_built()
    
    def block_built(self, x: int, y: int):
        pass # TODO
    
    def block_destroyed(self, x: int, y: int):
        pass # TODO

if __name__ == "__main__":
    AI().play()